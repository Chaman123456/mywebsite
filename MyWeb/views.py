from django.shortcuts import render, HttpResponse
from django.core.mail import send_mail
from threading import Thread


# Create your views here.
def home(request):
    value = {
        "home_page" : "active"
    }
    return render(request, 'Home.html', value)

def about(request):
    value = {
        "about_page" : "active"
    }
    return render(request, 'About.html', value)

def blog(request):
    value = {
        "blog_page" : "active"
    }
    return render(request, 'Blog.html', value)

def contactUs(request):
    value = {
        "contact_page" : "active"
    }
    return render(request, 'Contact.html', value)

def project(request):
    value = {
        "project_page" : "active"
    }
    return render(request, 'Project.html', value)

def thankyou(request):
    send_mail("From: %s"%request.POST.get("userEmail"), request.POST.get("userMesaage"), "", ['chaman.sharma1988@gmail.com'])
    return render(request, 'Thankyou.html')

    
