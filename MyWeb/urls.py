from django.contrib import admin
from django.urls import path, include
from MyWeb import views

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('blog/', views.blog, name='blog'),
    path('contact/', views.contactUs, name='contactUs'),
    path('project/', views.project, name='project'),
    path('thankyou/', views.thankyou, name='thankyou')
]
